package angeloid.dreamnarae.Benchmark;

import org.holoeverywhere.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MotionEvent;

public abstract class Tester extends Activity {
	public String TAG, mSourceTag = "unknown";
	public final static String PACKAGE = "angeloid.dreamnarae.Benchmark";
	public int mRound, mNow, mIndex;
	public long mTesterStart = 0, mTesterEnd = 0;

	public abstract String getTag();

	public abstract int sleepBeforeStart();

	public abstract int sleepBetweenRound();

	public abstract void oneRound();

	public boolean mNextRound = true, mDropTouchEvent = true, mDropTrackballEvent = true;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		TAG = getTag();
		Intent intent = getIntent();
		if (intent != null) {
			mRound = Case.getRound(intent);
			mSourceTag = Case.getSource(intent);
			mIndex = Case.getIndex(intent);
		} else {
			mRound = 80;
			mIndex = -1;
		}
		mNow = mRound;
	}

	@Override
	public void onPause() {
		super.onPause();
		interruptTester();
	}

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
		if (mDropTouchEvent) {
			return false;
		} else {
			return super.dispatchTouchEvent(ev);
		}
	}

	@Override
	public boolean dispatchTrackballEvent(MotionEvent ev) {
		if (mDropTrackballEvent) {
			return false;
		} else {
			return super.dispatchTouchEvent(ev);
		}
	}

	public void startTester() {
		TesterThread thread = new TesterThread(sleepBeforeStart(), sleepBetweenRound());
		thread.start();
	}

	public void interruptTester() {
		mNow = 0;
		finish();
	}

	public void finishTester(long start, long end) {
		mTesterStart = start;
		mTesterEnd = end;
		Intent intent = new Intent();
		if (mSourceTag == null || mSourceTag.equals("")) {
			Case.putSource(intent, "unknown");
		} else {
			Case.putSource(intent, mSourceTag);
		}
		Case.putIndex(intent, mIndex);
		saveResult(intent);
		setResult(0, intent);
		finish();
	}

	protected boolean saveResult(Intent intent) {
		long elapse = mTesterEnd - mTesterStart;
		Case.putResult(intent, elapse);
		return true;
	}

	public void resetCounter() {
		mNow = mRound;
	}

	public void decreaseCounter() {
		mNow = mNow - 1;
		mNextRound = true;
	}

	public boolean isTesterFinished() {
		return (mNow <= 0);
	}

	class TesterThread extends Thread {
		int mSleepingStart;
		int mSleepingTime;

		TesterThread(int sleepStart, int sleepPeriod) {
			mSleepingStart = sleepStart;
			mSleepingTime = sleepPeriod;
		}

		public void lazyLoop() throws Exception {
			while (!isTesterFinished()) {
				if (mNextRound) {
					mNextRound = false;
					oneRound();
				} else {
					sleep(mSleepingTime);
				}
			}
		}

		public void nervousLoop() throws Exception {
			while (!isTesterFinished()) {
				oneRound();
			}
		}

		public void sleepLoop() throws Exception {
			while (!isTesterFinished()) {
				oneRound();
				sleep(mSleepingTime);
			}
		}

		public void run() {
			try {
				sleep(mSleepingStart);
				long start = SystemClock.uptimeMillis();
				lazyLoop();
				long end = SystemClock.uptimeMillis();
				finishTester(start, end);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
