package angeloid.dreamnarae.v4;

import java.util.ArrayList;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import angeloid.Narae.NaraePreference;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import angeloid.dreamnarae.Benchmark.BenchMarkTester;

public class CompatibleActivity extends BaseListViewActivity {
	public ListView list;
	public ListAdapter adapter;
	public static ArrayList<String> bench_title;
	public static ArrayList<Drawable> background;
	public static ArrayList<Drawable> icon;
	public static ArrayList<String> bench_subtitle;
	public NaraePreference np;
	public int result;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		init();
		setContentView(R.layout.menulist);
		bench_title = new ArrayList<String>();
		bench_subtitle = new ArrayList<String>();
		background = new ArrayList<Drawable>();
		icon = new ArrayList<Drawable>();
		ab.setTitle(R.string.activity_compatible);
		np = new NaraePreference(CompatibleActivity.this);
		list = (ListView) findViewById(R.id.listView1);
		View header = getLayoutInflater().inflate(R.layout.compatible, null);
		TextView text = (TextView) header.findViewById(R.id.textView1);
		result = np.getValue("result", 0);
		text.setText(String.valueOf(result + "ms"));
		list.addHeaderView(header);
		adapter = new ListAdapter(CompatibleActivity.this, pd);
		new LoadData().execute();
		ab.addTab(Economy.setTabListener(new FakeListener()));
		ab.addTab(Tweak.setTabListener(new FakeListener()));
		ab.setSelectedNavigationItem(1);
		Economy.setIcon(R.drawable.ic_economy_disable);
		Tweak.setIcon(R.drawable.ic_tweak_enable);
		Economy.setTabListener(new TabListener());
		Tweak.setTabListener(new TabListener());
	}
	
	public class FakeListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {
				Economy.setIcon(R.drawable.ic_economy_enable);
				Tweak.setIcon(R.drawable.ic_tweak_disable);
				Intent a = new Intent(CompatibleActivity.this, MainActivity.class);
				a.putExtra("value", 0);
				startActivity(a);
			} else if (i == 1) {}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}


	@Override
	public void onDestroy() {
		am.removeActivity(this);
		super.onDestroy();
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 1) {
				Intent i = new Intent(CompatibleActivity.this, BenchMarkTester.class);
				startActivity(i);
			} else if (position == 5) {
				AlertDialog.Builder b = new AlertDialog.Builder(CompatibleActivity.this);
				WebView wv = new WebView(CompatibleActivity.this);
				if (np.getValue("lang", "en").equals("ko")) {
					wv.loadUrl("file:///android_asset/warning/benchmark.html");
				} else {
					wv.loadUrl("file:///android_asset/warning/benchmark_en.html");
				}
				wv.setWebViewClient(new WebViewClient() {
					@Override
					public boolean shouldOverrideUrlLoading(WebView view, String url) {
						view.loadUrl(url);
						return true;
					}
				});
				b.setView(wv);
				b.setPositiveButton(android.R.string.cancel, new OnClickListener() {
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						arg0.dismiss();
					}
				});
				b.show();
			}
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			if (result == 0) {
				bench_title.add(getResources().getString(R.string.list_mass));
				icon.add(getResources().getDrawable(R.drawable.list_play));
				bench_subtitle.add(getResources().getString(R.string.info_mass));
			} else {
				bench_title.add(getResources().getString(R.string.list_refresh));
				icon.add(getResources().getDrawable(R.drawable.list_refresh));
				bench_subtitle.add(getResources().getString(R.string.info_refresh));
			}
			background.add(getResources().getDrawable(R.drawable.darkorange));
			bench_title.add(getResources().getString(R.string.list_device));
			bench_subtitle.add(Build.MANUFACTURER + " " + Build.MODEL + " " + Build.VERSION.RELEASE);
			background.add(getResources().getDrawable(R.drawable.royalblue));
			icon.add(getResources().getDrawable(R.drawable.list_device));
			bench_title.add(getResources().getString(R.string.list_tweak));
			bench_subtitle.add(np.getValue("mode", getResources().getString(R.string.select_sdcard)));
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			icon.add(getResources().getDrawable(R.drawable.list_usetweak));
			bench_title.add(getResources().getString(R.string.list_last));
			bench_subtitle.add(np.getValue("last_benchmark", getResources().getString(R.string.select_sdcard)));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			icon.add(getResources().getDrawable(R.drawable.list_time));
			bench_title.add(getResources().getString(R.string.list_help));
			bench_subtitle.add(getResources().getString(R.string.info_help));
			background.add(getResources().getDrawable(R.drawable.dimgray));
			icon.add(getResources().getDrawable(R.drawable.ic_help));
			for (int i = 0; i < bench_title.size(); i++) {
				adapter.add(new Menu(bench_title.get(i), bench_subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}
	}
}
