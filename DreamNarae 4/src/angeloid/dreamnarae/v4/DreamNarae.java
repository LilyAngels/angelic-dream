package angeloid.dreamnarae.v4;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.holoeverywhere.app.Application;
import android.annotation.SuppressLint;
import android.os.Build;
import android.os.StrictMode;
import android.util.Log;

/**
 * 드림나래 개발자모드
 * 
 * @author WindSekirun
 */
public class DreamNarae extends Application {
	public static int DEVELOPER_MODE = 0;
	public static String LOGTAG = "DreamNarae";
	public static String MODE_DEVELOPER = "DEVELOPER_MODE";
	public static String MODE_USER = "USER_MODE";

	@Override
	public void onCreate() {
		super.onCreate();
		if (DEVELOPER_MODE == 1) {
			try {
				DEVELOPER();
				Log.e(LOGTAG, MODE_DEVELOPER);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			Log.e(LOGTAG, MODE_USER);
		}
	}

	@SuppressLint("NewApi")
	public void DEVELOPER() {
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
			try {
				Class<?> strictModeClass = Class.forName("android.os.StrictMode", true, Thread.currentThread().getContextClassLoader());
				Class<?> threadPolicyClass = Class.forName("android.os.StrictMode$ThreadPolicy", true, Thread.currentThread()
						.getContextClassLoader());
				Class<?> threadPolicyBuilderClass = Class.forName("android.os.StrictMode$ThreadPolicy$Builder", true, Thread
						.currentThread().getContextClassLoader());
				Method setThreadPolicyMethod = strictModeClass.getMethod("setThreadPolicy", threadPolicyClass);
				Method detectAllMethod = threadPolicyBuilderClass.getMethod("detectAll");
				Method penaltyMethod = threadPolicyBuilderClass.getMethod("penaltyLog");
				Method buildMethod = threadPolicyBuilderClass.getMethod("build");
				Constructor<?> threadPolicyBuilderConstructor = threadPolicyBuilderClass.getConstructor();
				Object threadPolicyBuilderObject = threadPolicyBuilderConstructor.newInstance();
				Object obj = detectAllMethod.invoke(threadPolicyBuilderObject);
				obj = penaltyMethod.invoke(obj);
				Object threadPolicyObject = buildMethod.invoke(obj);
				setThreadPolicyMethod.invoke(strictModeClass, threadPolicyObject);
				if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB_MR2) {
					StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().build());
					StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().penaltyLog().build());
				} else {
					StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectAll().penaltyLog().penaltyFlashScreen().build());
					StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects().detectLeakedClosableObjects()
							.setClassInstanceLimit(Class.forName("angeloid.dreamnarae.v4.MainActivity"), 200).penaltyLog().build());
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		} else {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().detectAll().penaltyDeath().penaltyDeathOnNetwork()
					.penaltyDialog().penaltyDropBox().penaltyFlashScreen().penaltyLog().permitAll().build();
			StrictMode.setThreadPolicy(policy);
			try {
				StrictMode.VmPolicy vm = new StrictMode.VmPolicy.Builder().detectAll().detectLeakedSqlLiteObjects().penaltyDeath()
						.penaltyDropBox().penaltyLog().setClassInstanceLimit(Class.forName("angeloid.dreamnarae.v4.DreamNarae"), 200)
						.build();
				StrictMode.setVmPolicy(vm);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
}
