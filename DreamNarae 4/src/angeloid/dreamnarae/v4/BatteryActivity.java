package angeloid.dreamnarae.v4;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import org.holoeverywhere.app.AlertDialog;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import angeloid.dreamnarae.Base.BaseListViewActivity;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
@SuppressLint({ "NewApi", "HandlerLeak" })
public class BatteryActivity extends BaseListViewActivity {
	public ActionBar.TabListener tabListener;
	public ListView list;
	public static ArrayList<String> battery_title;
	public static ArrayList<String> battery_subtitle;
	public WifiManager wifi;
	public BluetoothAdapter mBTAdapter;
	public IntentFilter ifilter;
	public int Status, Tem, Vol;
	public String Tech;
	public float level;
	public Handler mh;

	@Override
	public void onCreate(Bundle s) {
		super.onCreate(s);
		am.addActivity(this);
		setContentView(R.layout.menulist);
		init();
		this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		list = (ListView) findViewById(R.id.listView1);
		View header = getLayoutInflater().inflate(R.layout.battery, null);
		list.addHeaderView(header);
		new LoadData().execute();
		ab.addTab(Economy.setTabListener(new TabListener()));
		ab.addTab(Tweak.setTabListener(new TabListener()));
		mh = new Handler() {
			public void handleMessage(Message msg) {
				super.handleMessage(msg);
				Crouton.makeText(BatteryActivity.this, getResources().getString(R.string.crouton_already), Style.INFO).show();
			}
		};
	}

	@Override
	public void onDestroy() {
		am.removeActivity(this);
		unregisterReceiver(mBatInfoReceiver);
		Crouton.cancelAllCroutons();
		super.onDestroy();
	}

	public class ClickMenu implements AdapterView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			if (position == 1) {
				AlertDialog.Builder d = new AlertDialog.Builder(BatteryActivity.this);
				View v = getLayoutInflater().inflate(R.layout.batterypopup);
				TextView type = (TextView) v.findViewById(R.id.type);
				TextView temp = (TextView) v.findViewById(R.id.temp);
				TextView vol = (TextView) v.findViewById(R.id.vol);
				TextView levelo = (TextView) v.findViewById(R.id.level);
				temp.setText(String.valueOf(Tem / 10) + "��C");
				vol.setText(String.valueOf(Vol + "mV"));
				type.setText(Tech);
				levelo.setText(String.valueOf(level));
				d.setView(v);
				d.setPositiveButton(android.R.string.cancel, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				d.show();
			} else if (position == 2) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BatteryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						closeWifi();
						closeBlueTooth();
						closeMobile();
						Crouton.makeText(BatteryActivity.this, getResources().getString(R.string.crouton_networkoff), Style.INFO).show();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
			} else if (position == 3) {
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
					startActivityForResult(new Intent(android.provider.Settings.ACTION_AIRPLANE_MODE_SETTINGS), 0);
				} else {
					AlertDialog.Builder builder = new AlertDialog.Builder(BatteryActivity.this);
					builder.setMessage(getString(R.string.alert_run));
					builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							RunAirplaneMode();
						}
					});
					builder.setNegativeButton(android.R.string.no, new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});
					builder.show();
				}
			} else if (position == 4) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BatteryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						RunRotateScreen();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
			} else if (position == 5) {
				AlertDialog.Builder builder = new AlertDialog.Builder(BatteryActivity.this);
				builder.setMessage(getString(R.string.alert_run));
				builder.setPositiveButton(android.R.string.yes, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						RunTouchFeedback();
					}
				});
				builder.setNegativeButton(android.R.string.no, new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.show();
			}
		}
	}

	private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context c, Intent i) {
			Vol = i.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);
			Tem = i.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
			Tech = i.getStringExtra("technology");
			int l = i.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
			int s = i.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
			if (l == -1 || s == -1) {
				level = 50.0f;
			}
			level = ((float) l / (float) s) * 100.0f;
		}
	};

	public class TabListener implements ActionBar.TabListener {
		public void onTabSelected(Tab tab, FragmentTransaction ft) {
			int i = tab.getPosition();
			if (i == 0) {} else if (i == 1) {
				Economy.setIcon(R.drawable.ic_economy_disable);
				Tweak.setIcon(R.drawable.ic_tweak_enable);
				Intent a = new Intent(BatteryActivity.this, MainActivity.class);
				a.putExtra("value", 1);
				startActivity(a);
			}
		}

		public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		}

		public void onTabReselected(Tab tab, FragmentTransaction ft) {
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			list.setAdapter(adapter);
			list.setOnItemClickListener(new ClickMenu());
			super.onPostExecute(result);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			battery_title = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.battery_menu_title)));
			battery_subtitle = new ArrayList<String>( Arrays.asList(getResources().getStringArray(R.array.battery_menu_subtitle)));
			ArrayList<Drawable> icon = new ArrayList<Drawable>();
			ArrayList<Drawable> background = new ArrayList<Drawable>();
			background.add(getResources().getDrawable(R.drawable.aquamarine));
			background.add(getResources().getDrawable(R.drawable.royalblue));
			background.add(getResources().getDrawable(R.drawable.mediumorchid));
			background.add(getResources().getDrawable(R.drawable.firebrick));
			background.add(getResources().getDrawable(R.drawable.dimgray_bold));
			icon.add(getResources().getDrawable(R.drawable.list_batteryinfo));
			icon.add(getResources().getDrawable(R.drawable.list_network));
			icon.add(getResources().getDrawable(R.drawable.list_airplane));
			icon.add(getResources().getDrawable(R.drawable.list_rotate));
			icon.add(getResources().getDrawable(R.drawable.list_feedback));
			for (int i = 0; i < battery_title.size(); i++) {
				adapter.add(new Menu(battery_title.get(i), battery_subtitle.get(i), background.get(i), icon.get(i)));
			}
			return null;
		}
	}

	public void closeWifi() {
		wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		if (wifi.isWifiEnabled()) {
			wifi.setWifiEnabled(false);
		} else {}
	}

	public void RunTouchFeedback() {
		if (isTouchFeedback() == false) {
			mh.sendEmptyMessage(0);
		} else {
			Settings.System.putInt(getContentResolver(), Settings.System.HAPTIC_FEEDBACK_ENABLED, 0);
			Crouton.makeText(this, getResources().getString(R.string.crouton_feedbackoff), Style.INFO).show();
		}
	}

	public boolean isTouchFeedback() {
		int tab = Settings.System.getInt(getContentResolver(), Settings.System.HAPTIC_FEEDBACK_ENABLED, 0);
		if (tab == 0) {
			return false;
		} else {
			return true;
		}
	}

	public void closeBlueTooth() {
		mBTAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBTAdapter.isEnabled()) {
			mBTAdapter.disable();
		}
	}

	public void closeMobile() {
		try {
			ConnectivityManager conman = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
			Class<?> conmanClass = Class.forName(conman.getClass().getName());
			Field iConnectivityManagerField = conmanClass.getDeclaredField("mService");
			iConnectivityManagerField.setAccessible(true);
			Object iConnectivityManager = iConnectivityManagerField.get(conman);
			Class<?> iConnectivityManagerClass = Class.forName(iConnectivityManager.getClass().getName());
			Method setMobileDataEnabledMethod = iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
			setMobileDataEnabledMethod.setAccessible(true);
			setMobileDataEnabledMethod.invoke(iConnectivityManager, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("deprecation")
	public void RunAirplaneMode() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			if (isAirplaneMode() == false) {
				Settings.Global.putString(getContentResolver(), "airplane_mode_on", "1");
				Crouton.makeText(this, getResources().getString(R.string.crouton_airon), Style.INFO).show();
			} else {
				mh.sendEmptyMessage(0);
			}
		} else {
			if (isAirplaneMode() == false) {
				Settings.System.putInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 1);
				Crouton.makeText(this, getResources().getString(R.string.crouton_airon), Style.INFO).show();
			} else {}
		}
	}

	public void RunRotateScreen() {
		if (isRotateMode() == true) {
			Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
			Crouton.makeText(this, getResources().getString(R.string.crouton_rotateoff), Style.INFO).show();
		} else {
			mh.sendEmptyMessage(0);
		}
	}

	public boolean isRotateMode() {
		int isto = Settings.System.getInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
		if (isto == 0) {
			return false;
		} else {
			return true;
		}
	}

	@SuppressLint("InlinedApi")
	public boolean isAirplaneMode() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
			String isto = Settings.Global.getString(getContentResolver(), Settings.Global.AIRPLANE_MODE_ON);
			if (!(isto.equals("airplane_mode_on"))) {
				return false;
			} else {
				return true;
			}
		} else {
			@SuppressWarnings("deprecation")
			boolean isEnabled = Settings.System.getInt(getContentResolver(), Settings.System.AIRPLANE_MODE_ON, 0) == 1;
			return isEnabled;
		}
	}
}
