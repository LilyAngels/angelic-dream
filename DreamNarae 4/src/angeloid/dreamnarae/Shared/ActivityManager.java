package angeloid.dreamnarae.Shared;

import java.util.ArrayList;
import android.app.Activity;

public class ActivityManager {
	public static ActivityManager activityMananger = null;
	public ArrayList<Activity> activityList = null;

	public ActivityManager() {
		activityList = new ArrayList<Activity>();
	}

	public static ActivityManager getInstance() {
		if (ActivityManager.activityMananger == null) {
			activityMananger = new ActivityManager();
		}
		return activityMananger;
	}

	public void addActivity(Activity activity) {
		activityList.add(activity);
	}

	public boolean removeActivity(Activity activity) {
		return activityList.remove(activity);
	}

	public void finishAllActivity() {
		for (Activity activity : activityList) {
			activity.finish();
		}
	}
}
