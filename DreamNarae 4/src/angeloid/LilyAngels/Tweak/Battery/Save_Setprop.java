package angeloid.LilyAngels.Tweak.Battery;

public class Save_Setprop {
	public static String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		sb.append("setprop enable.frequency.save 10");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop enable.NW:NW.operate Hand-over");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop enable.sf.cache.type memory");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop enale.nextAction.reckon earlier");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop persist.sys.ui.hw 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop persist.sys.use_dithering 0");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop persist.text.type clear");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop pm.sleep_mode 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop proximity_incall enable");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.battery.voltage.cooperate true");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.cache.memory.cooperate true");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.config.cpuui.rendering false");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.config.hw_power_saving 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.config.hw_quickpoweron true");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.connect.action slippery");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.dev.dmm.dpd.start_address 0");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.ext4fs 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.hwui.render_dirty_region false");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.kernel.android.checkjni 0");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.min_pointer_dur 10");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.product.process.RAM.save 2");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.product.sametime.delay false");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.product.use_charge_counter 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.ril.disable.power.collapse 0");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.ril.sensor.sleep.control 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.screen.rendering false");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.sf.cache.type memory");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.sf.compbypass.enable 1");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop ro.sf.data.suspend.type n");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop windowsmgr.max_events_per_sec 300");
		sb.append(System.getProperty("line.separator"));
		sb.append("setprop wifi.supplicant_scan_interval 240");
		sb.append(System.getProperty("line.separator"));
		String string = sb.toString();
		return string;
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		sb.append("narae enable.frequency.save 10 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae enable.NW:NW.operate Hand-over ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae enable.sf.cache.type memory ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae enale.nextAction.reckon earlier ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae persist.sys.ui.hw 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae persist.sys.use_dithering 0 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae persist.text.type clear ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae pm.sleep_mode 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae proximity_incall enable ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.battery.voltage.cooperate true ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.cache.memory.cooperate true ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.config.cpuui.rendering false ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.config.hw_power_saving 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.config.hw_quickpoweron true ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.connect.action slippery ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.dev.dmm.dpd.start_address 0 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.ext4fs 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.hwui.render_dirty_region false ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.kernel.android.checkjni 0 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.min_pointer_dur 10 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.product.process.RAM.save 2 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.product.sametime.delay false ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.product.use_charge_counter 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.ril.disable.power.collapse 0 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.ril.sensor.sleep.control 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.screen.rendering false ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.sf.cache.type memory ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.sf.compbypass.enable 1 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae ro.sf.data.suspend.type n ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae windowsmgr.max_events_per_sec 300 ");
		sb.append(System.getProperty("line.separator"));
		sb.append("narae wifi.supplicant_scan_interval 240 ");
		sb.append(System.getProperty("line.separator"));
		String string = sb.toString();
		return string;
	}
}
