package angeloid.LilyAngels.Tweak.Battery;

import java.util.ArrayList;
import java.util.Scanner;

public class Save {
	public static ArrayList<String> data;
	public static Scanner s;

	public String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		sb.append(Save_Composition.getTweak(false));
		sb.append(Save_CpuLevel.getTweak());
		sb.append(Save_Execution.getTweak(false));
		sb.append(Save_RM.getTweak());
		sb.append(Save_SDCard.getTweak());
		sb.append(Save_Setprop.getTweak(false));
		sb.append(Save_Sleep.getTweak(false));
		sb.append(Save_Swap.getTweak(60));
		sb.append(Save_VM.getTweak());
		return sb.toString();
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		sb.append(Save_Composition.getTweak(true));
		sb.append(Save_CpuLevel.getTweak());
		sb.append(Save_Execution.getTweak(true));
		sb.append(Save_RM.getTweak());
		sb.append(Save_SDCard.getTweak());
		sb.append(Save_Setprop.getTweak(true));
		sb.append(Save_Sleep.getTweak(true));
		sb.append(Save_Swap.getTweak(60));
		sb.append(Save_VM.getTweak());
		return sb.toString();
	}

	public static Double getSize() {
		s = new Scanner(Setprop());
		while (s.hasNext()) {
			data.add(Setprop());
		}
		Double result = (double) data.size();
		return result;
	}
}
