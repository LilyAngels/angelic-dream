package angeloid.LilyAngels.Tweak.Addon;

public class ZipAlign {
	public static String getTweak() {
		StringBuilder sb = new StringBuilder();
		sb.append("for DIR in /system/app /data/app; do");
		sb.append(System.getProperty("line.separator"));
		sb.append("cd $DIR;");
		sb.append(System.getProperty("line.separator"));
		sb.append("for APK in *.apk; do");
		sb.append(System.getProperty("line.separator"));
		sb.append("	if [ $APK -ot $ZIPALIGNDB ] && [ $(grep \"$DIR/$APK\" $ZIPALIGNDB|wc -l) -gt 0 ]; then");
		sb.append(System.getProperty("line.separator"));
		sb.append("	else");
		sb.append(System.getProperty("line.separator"));
		sb.append("		ZIPCHECK=`/system/xbin/zipalign -c -v 4 $APK | grep FAILED | wc -l`;");
		sb.append(System.getProperty("line.separator"));
		sb.append("		if [ $ZIPCHECK == \"1\" ]; then");
		sb.append(System.getProperty("line.separator"));
		sb.append("				/system/xbin/zipalign -v -f 4 $APK /sdcard/download/$APK;");
		sb.append(System.getProperty("line.separator"));
		sb.append("			busybox mount -o rw,remount /system;");
		sb.append(System.getProperty("line.separator"));
		sb.append("			cp -f -p /sdcard/download/$APK $APK;");
		sb.append(System.getProperty("line.separator"));
		sb.append("				grep \"$DIR/$APK\" $ZIPALIGNDB > /dev/null");
		sb.append(System.getProperty("line.separator"));
		sb.append("			else");
		sb.append(System.getProperty("line.separator"));
		sb.append("				grep \"$DIR/$APK\" $ZIPALIGNDB > /dev/null");
		sb.append(System.getProperty("line.separator"));
		sb.append("			fi;");
		sb.append(System.getProperty("line.separator"));
		sb.append("	fi;");
		sb.append(System.getProperty("line.separator"));
		sb.append("	done;");
		sb.append(System.getProperty("line.separator"));
		sb.append("done;");
		sb.append(System.getProperty("line.separator"));
		sb.append("");
		sb.append(System.getProperty("line.separator"));
		sb.append("busybox mount -o ro,remount /system;");
		sb.append(System.getProperty("line.separator"));
		return sb.toString();
	}
}
