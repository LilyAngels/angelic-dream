package angeloid.LilyAngels.Tweak.Performance;

import java.util.ArrayList;
import java.util.Scanner;

public class Pure {
	public static ArrayList<String> data;
	public static Scanner s;

	public String getTweak(boolean isNarae) {
		String string = new String();
		if (isNarae = true) {
			string = Narae();
		} else {
			string = Setprop();
		}
		return string;
	}

	public static String Setprop() {
		StringBuilder sb = new StringBuilder();
		sb.append(Pure_Composition.getTweak(false));
		sb.append(Pure_CpuLevel.getTweak());
		sb.append(Pure_Execution.getTweak(false));
		sb.append(Pure_RM.getTweak());
		sb.append(Pure_SDCard.getTweak());
		sb.append(Pure_Setprop.getTweak(false));
		sb.append(Pure_Sleep.getTweak(false));
		sb.append(Pure_Swap.getTweak(60));
		sb.append(Pure_VM.getTweak());
		return sb.toString();
	}

	public static String Narae() {
		StringBuilder sb = new StringBuilder();
		sb.append(Pure_Composition.getTweak(true));
		sb.append(Pure_CpuLevel.getTweak());
		sb.append(Pure_Execution.getTweak(true));
		sb.append(Pure_RM.getTweak());
		sb.append(Pure_SDCard.getTweak());
		sb.append(Pure_Setprop.getTweak(true));
		sb.append(Pure_Sleep.getTweak(true));
		sb.append(Pure_Swap.getTweak(60));
		sb.append(Pure_VM.getTweak());
		return sb.toString();
	}

	public static Double getSize() {
		s = new Scanner(Setprop());
		while (s.hasNext()) {
			data.add(Setprop());
		}
		Double result = (double) data.size();
		return result;
	}
}
