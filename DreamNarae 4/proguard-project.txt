##############################################################
# Keep Class
##############################################################
-keep class de.keyboardsurfer.android.widget.crouton.**
-keep class com.stericson.**
-keep class org.holoeverywhere.**
-keep class org.apache.commons.**
##############################################################
# Keep Interface
##############################################################
-keep interface de.keyboardsurfer.android.widget.crouton.**
-keep interface com.stericson.**
-keep interface org.holoeverywhere.**
-keep interface org.apache.commons.**
##############################################################
# Don't Warn
##############################################################
-dontwarn de.keyboardsurfer.android.widget.crouton.**
-dontwarn com.stericson.**
-dontwarn org.holoeverywhere.**
-dontwarn org.apache.commons.**
##############################################################
# ETC Keep
##############################################################
-renamesourcefileattribute SourceFile
-keepattributes SourceFile
-keepattributes LineNumberTable
-keepattributes Signature
-keepattributes *Annotation*
-keepattributes InnerClasses
##############################################################
# Option
##############################################################
-optimizationpasses 25 
-dontusemixedcaseclassnames 
-dontskipnonpubliclibraryclasses
-dontskipnonpubliclibraryclassmembers
-dontskipnonpubliclibraryclassmembers
-dontpreverify 
-verbose 
-optimizations !code/simplification/arithmetic,!field/*,!class/merging/* 
-printmapping out.map
##############################################################
# Keep Components
##############################################################
-keep public class * extends android.app.Activity 
-keep public class * extends android.app.Application 
-keep public class * extends android.app.Service 
-keep public class * extends android.content.BroadcastReceiver 
-keep public class * extends android.content.ContentProvider 
-keep public class * extends android.app.backup.BackupAgentHelper 
-keep public class * extends android.preference.Preference 
-keep public class com.android.vending.licensing.ILicensingService
-keepclasseswithmembernames class * { 
    native <methods>; 
} 

-keepclasseswithmembers class * { 
    public <init>(android.content.Context, android.util.AttributeSet); 
} 

-keepclasseswithmembers class * { 
    public <init>(android.content.Context, android.util.AttributeSet, int); 
} 

-keepclassmembers class * extends android.app.Activity { 
   public void *(android.view.View); 
} 

-keepclassmembers enum * { 
    public static **[] values(); 
    public static ** valueOf(java.lang.String); 
} 

-keep class * implements android.os.Parcelable { 
  public static final android.os.Parcelable$Creator *;
  }
  
##############################################################
# External libs
##############################################################
-libraryjars libs/RootTools2.6.jar
-libraryjars libs/commons-io-2.4.jar